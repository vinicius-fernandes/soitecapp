package com.soirtec.soirtecapp.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.soirtec.soirtecapp.R;
import com.soirtec.soirtecapp.interfaces.ItemClickListener;
import com.soirtec.soirtecapp.model.Movie;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vinicius on 25/07/18.
 */
public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> movies;
    private Context context;

    // Consts
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private static final String DEFAULT_POSTER_MOVIE = "http://icons-for-free.com/free-icons/png/512/285640.png";

    private boolean isLoadingAdded = false;

    public MovieAdapter(Context context) {
        this.context = context;
        movies = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.item_list, parent, false);
        viewHolder = new MovieViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        Movie movie = movies.get(position); // Movie

        switch (getItemViewType(position)) {

            case ITEM:
                final MovieViewHolder movieViewHolder = (MovieViewHolder) holder;

                /**
                 * Using Glide to handle image loading.
                 * Learn more about Glide here:
                 */
                Glide
                        .with(context)
                        .load(movie.getPoster().isEmpty() ? DEFAULT_POSTER_MOVIE : movie.getPoster())
                        .into(movieViewHolder.mPosterImg);

                break;

            case LOADING:
                // Do nothing
                break;
        }

    }

    @Override
    public int getItemCount() {
        return movies == null ? 0 : movies.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == movies.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
    View Holders
    ________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    protected class MovieViewHolder extends RecyclerView.ViewHolder {

        private ImageView mPosterImg;

        public MovieViewHolder(View itemView) {
            super(itemView);
            mPosterImg = (ImageView) itemView.findViewById(R.id.movie_poster);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    /*
    Helpers
    ________________________________________________________________________________________________
    */

    public void add(Movie r) {
        movies.add(r);
        notifyItemInserted(movies.size() - 1);
    }

    public void addAll(List<Movie> moveResults) {
        for (Movie result : moveResults) {
            add(result);
        }
    }

    public void remove(Movie r) {
        int position = movies.indexOf(r);
        if (position > -1) {
            movies.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new Movie());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = movies.size() - 1;
        Movie result = getItem(position);

        if (result != null) {
            movies.remove(position);
            notifyItemRemoved(position);
        }
    }

    public Movie getItem(int position) {
        return movies.get(position);
    }

}
