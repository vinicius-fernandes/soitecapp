package com.soirtec.soirtecapp;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.soirtec.soirtecapp.adapters.MovieAdapter;
import com.soirtec.soirtecapp.interfaces.ItemClickListener;
import com.soirtec.soirtecapp.model.Movie;
import com.soirtec.soirtecapp.model.MoviesResponse;
import com.soirtec.soirtecapp.api.Service;
import com.soirtec.soirtecapp.api.Api;
import com.soirtec.soirtecapp.utils.PaginationScrollListener;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by vinicius on 25/07/18.
 */
public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final String TAG = "MainActivity";

    // Adapter
    MovieAdapter adapter;
    GridLayoutManager gridLayoutManager;

    // UI References
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ProgressDialog progressDialog;

    private static String querySearch;
    private static final int PAGE_START = 1;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 5;     // Limiting to 5 pages
    private int currentPage = PAGE_START;

    // Movie Service
    private Service movieService;

    public MainActivity() {
        //init service
        movieService = Api.getService();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        initComponentsView();

        progressBar.setVisibility(View.INVISIBLE);

        adapter = new MovieAdapter(this);

        gridLayoutManager = new GridLayoutManager(this, 3);

        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        setListener();
    }

    /**
     *
     */
    private void initComponentsView() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview_movies);
        progressBar = (ProgressBar) findViewById(R.id.main_progress_bar);
    }

    /**
     *
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setListener() {

        recyclerView.addOnScrollListener(new PaginationScrollListener(gridLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;

                // mocking network delay for API call
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadNextPage();
                    }
                }, 1000);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        recyclerView.addOnItemTouchListener(
                new ItemClickListener(getApplicationContext(), recyclerView ,new ItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {

                        Movie movie = adapter.getItem(position);

                        Intent intent = new Intent(getApplicationContext(), DetailActivity.class);
                        intent.putExtra("movie", movie);
                        startActivity(intent);
                    }

                    @Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }
                })
        );

    }

    /**
     *
     */
    private void loadFirstPage() {

        // clear the list to start a new search
        adapter.clear();

        Log.d(TAG, "loadFirstPage: ");
        progressBar.setVisibility(View.VISIBLE);
        progressDialog = ProgressDialog.show(this, "Aguarde um momento", "Buscando filmes" , true, false);

        callMoviesApi().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();

                if (response.isSuccessful()) {

                    List<Movie> movies = fetchResults(response);

                    if (movies != null) {
                        adapter.addAll(movies);

                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;

                    }  else
                        Toast.makeText(MainActivity.this, "Nenhum filme encontrado", Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();
                t.printStackTrace();
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     *
     */
    private void loadNextPage() {

        Log.d(TAG, "loadNextPage: " + currentPage);
        progressBar.setVisibility(View.VISIBLE);
        progressDialog = ProgressDialog.show(this, "Aguarde um momento", "Buscando filmes" , true, false);

        callMoviesApi().enqueue(new Callback<MoviesResponse>() {
            @Override
            public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();

                adapter.removeLoadingFooter();
                isLoading = false;

                if (response.isSuccessful()) {

                    List<Movie> movies = fetchResults(response);

                    if (movies != null) {
                        adapter.addAll(movies);

                        if (currentPage <= TOTAL_PAGES) adapter.addLoadingFooter();
                        else isLastPage = true;

                    } else
                        Toast.makeText(MainActivity.this, "Nenhum filme encontrado", Toast.LENGTH_SHORT).show();

                } else
                    Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<MoviesResponse> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
                progressDialog.dismiss();
                t.printStackTrace();
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * @param response extracts List<{@link Movie>} from response
     * @return
     */
    private List<Movie> fetchResults(Response<MoviesResponse> response) {
        MoviesResponse moviesResponse = response.body();
        return moviesResponse.getMovies();
    }

    /**
     * Performs a Retrofit call to the movies API.
     * Same API call for Pagination.
     * As {@link #currentPage} will be incremented automatically
     * by @{@link PaginationScrollListener} to load next page.
     */
    private Call<MoviesResponse> callMoviesApi() {
        return movieService.getMovies(Api.API_KEY, querySearch, currentPage);
    }

    /**
     *
     */
    private void exitApp() {
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        //Get the component.
        SearchView mSearchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();

        //Defines a help text:
        mSearchView.setQueryHint("Buscar filme...");

        // initialize the listener:
        mSearchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_quit) {
            exitApp();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        querySearch = query;
        loadFirstPage();
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

}
